/**
 * Combine all reducers in this file and export the combined reducers.
 */
import { combineReducers } from 'redux';
import auth from './login/reducer';

const IndexReducer = combineReducers({
  auth,
});

export default IndexReducer;
