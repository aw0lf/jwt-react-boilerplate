import React from 'react';
import  { Link } from "react-router-dom";

export default () => <Link to='/login'>Sign in</Link>;
