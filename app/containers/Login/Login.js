import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import {
  Button, Col, Form, FormGroup, Label, Input,
} from 'reactstrap';
import * as actions from '../../redux/login/actions';


class Login extends Component {
    state = {
      email: '',
      password: '',
      validate: {
        email: null,
        password: null,
      },
    };

    handleOnChange = e => {
        e.preventDefault();
        this.setState({ [e.target.name]: e.target.value });
    };

  handleSubmit = (e) => {
    const { email, password } = this.state;
    if (!this.isValid([this.validateEmail(email), this.validatePassword(password)])) {
      this.props.onAuth(email, password);
    }
  };

    isValid = values => values.every(val => val === true);

    validatePassword = (value) => {
      const regex = /[\s\S]{6,16}/;
      const { validate } = this.state;
      validate.email = regex.test(value);
      this.setState({ validate });
      return validate.email;
    };

    validateEmail = (value) => {
      const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      const { validate } = this.state;
      validate.email = regex.test(value);
      this.setState({ validate });
      return validate.email;
    };

    render() {
        if (this.props.isAuthenticated) {
            return <Redirect to="/" />;
        }

      let errorMessage = null;
      if (this.props.error) {
        errorMessage = (
                  <p>{this.props.error.message}</p>
        );
      }

      return (
             <div>
                {errorMessage}
                  {
                    this.props.loading
                      ? <p>loading</p>
                      : (
                            <Form onSubmit={this.handleSubmit}>
                                <Col md={{ size: 6, offset: 3 }}>
                                <FormGroup>
                                    <Label for="exampleEmail">Email</Label>
                                    <Input onChange={this.handleOnChange} valid={this.state.validate.email} type="email" name="email" id="exampleEmail" placeholder="with a placeholder" />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="examplePassword">Password</Label>
                                    <Input onChange={this.handleOnChange} type="password" name="password" id="examplePassword" placeholder="password placeholder" />
                                    <Button type="submit"> Submit</Button>
                                </FormGroup>
                                </Col>
                            </Form>
                      )
                     }
                 </div>
      );
    }
}

const mapStateToProps = state => ({
  loading: state.auth.loading,
  error: state.auth.error,
  isAuthenticated: state.auth.token !== null,
});

const mapDispatchToProps = dispatch => ({
  onAuth: (username, password) => dispatch(actions.authLogin(username, password)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
