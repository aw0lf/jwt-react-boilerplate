import React, { PureComponent } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import * as actions from '../../redux/login/actions';

class App extends PureComponent {
  render() {
    const { children } = this.props;
    return (
      <div>
        <Header />
        <div>
          {children}
        </div>
        <Footer />
      </div>
    );
  }
}
App.propTypes = {
  children: PropTypes.node.isRequired,
};
const mapDispatchToProps = dispatch => (
  { logout: () => dispatch(actions.logout()) }
);

export default withRouter(connect(null, mapDispatchToProps)(App));
