import React, { Component } from 'react';
import {
  Route, Switch, BrowserRouter, Redirect,
} from 'react-router-dom';
import { connect } from 'react-redux';
import App from './containers/App/App';
import Login from './containers/Login/Login';
import * as actions from './redux/login/actions';
import HomePage from './containers/HomePage/HomePage';


class RootRoutes extends Component {
  componentDidMount() {
    this.props.authCheckState();
  }

    PrivateRoute = ({ component: ChildComponent, ...rest }) => <Route {...rest} render={(props) => {
      if (this.props.loading) {
        return <em>Loading...</em>;
      } if (!this.props.isAuthenticated) {
        return <Redirect to="/login" />;
      }
      return <ChildComponent {...props} />;
    }} />;

    render() {
      const { PrivateRoute } = this;
      return (
            <BrowserRouter>
                <App >
                    <Switch>
                        <Route exact path="/login" component={Login} />
                        <PrivateRoute exact path="/" component={HomePage}/>
                    </Switch>
                </App>
            </BrowserRouter>
      );
    }
}
const mapStateToProps = state => ({
  isAuthenticated: state.token !== null,
});

const mapDispatchToProps = dispatch => ({
  authCheckState: () => dispatch(actions.authCheckState()),
});

export default connect(mapStateToProps, mapDispatchToProps)(RootRoutes);
