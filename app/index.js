import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';

import RootRoutes from './routes';
import store from './redux/store';
import 'bootstrap/dist/css/bootstrap.min.css';

const MOUNT_NODE = document.getElementById('app');

const render = () => {
  ReactDOM.render(
    <Provider store={store}>
      <RootRoutes />

    </Provider>,
    MOUNT_NODE,
  );
};

if (module.hot) {
  module.hot.accept(['./containers/App/App'], () => {
    ReactDOM.unmountComponentAtNode(MOUNT_NODE);
    render();
  });
}

render();
